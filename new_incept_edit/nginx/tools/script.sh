apt-get update
apt-get install -y nginx
mkdir -p /etc/nginx/ssl_certs/
cp /private.pem /etc/nginx/ssl_certs/
cp /public.key /etc/nginx/ssl_certs/
service nginx start
#apt-get install mariadb-server -y
service mysql start
mv default /etc/nginx/sites-available/default
chown -R www-data /var/www/*
chmod -R 755 /var/www/*