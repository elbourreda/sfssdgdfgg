apt-get update
apt-get install -y php-fpm php-mysql wget curl unzip
service php7.3-fpm start
mkdir -p /run/php/
sed -i 's/;daemonize = yes/daemonize = no/g' /etc/php/7.3/fpm/pool.d/www.conf
echo "listen = 9000" >> /etc/php/7.3/fpm/pool.d/www.conf
mkdir -p /var/www/html
cp wp-config.php /var/www/html/
cd /var/www/html/
wget https://wordpress.org/latest.zip
unzip latest.zip 
mv wordpress/* .