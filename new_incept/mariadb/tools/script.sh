apt-get update
apt-get install -y default-mysql-server
mkdir -p /var/run/mysqld/
chown mysql /var/run/mysqld /var/lib
chmod -R 755 /var/run/mysqld/ /var/lib/
cp my.cnf /etc/mysql/
service mysql start

mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO '${MSQL_USER}'@'localhost' IDENTIFIED BY '${MSQL_USER}';";
mysql -u root -e "GRANT ALL ON *.* to 'fbouibao'@'172.13.37.2' IDENTIFIED BY 'fbouibao';";
mysql --password=${MSQL_USER} --user=${MSQL_USER} -e "CREATE DATABASE wordpress;";
mysql --password=${MSQL_USER} --user=${MSQL_USER} -e  "FLUSH PRIVILEGES;";
# service mysql stop
mysql --password=${MSQL_USER} --user=${MSQL_USER} wordpress < /wp.sql


# INSERT INTO `wordpress`.`wp_users` ( `user_login`, `user_pass`, `user_email`, `user_status`, `display_name`) VALUES ('admin0', 'admin0', 'admin@reda3.com', '1', 'reda3 User');