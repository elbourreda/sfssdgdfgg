apt-get update
apt-get install -y php-fpm php-mysql wget curl unzip
service php7.3-fpm start
mkdir -p /run/php/
sed -i 's/;daemonize = yes/daemonize = no/g' /etc/php/7.3/fpm/pool.d/www.conf
echo "listen = 9000" >> /etc/php/7.3/fpm/pool.d/www.conf
mkdir -p /var/www/html
cp wp-config.php /var/www/html/
cd /var/www/html/
wget https://wordpress.org/latest.zip
wget https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
mv wp-cli.phar wp
chmod +x wp
unzip latest.zip && rm -rf latest.zip
mv wordpress/* . && rm -rf wordpress